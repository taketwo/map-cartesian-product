/* eslint-env mocha */

'use strict'

var expect = require('chai').expect
var product = require('../map-cartesian-product')

describe('#product', function () {
  it('should be a function', function () {
    expect(product).to.be.a('function')
  })

  it('should return an array of empty maps when input is empty', function () {
    expect(product({})).to.deep.equal([{}])
  })

  it('should not accept input that is not a map', function () {
    expect(product.bind(product, 'foobar')).to.throw(TypeError)
    expect(product.bind(product, [])).to.throw(TypeError)
    expect(product.bind(product, [1, 2, 3])).to.throw(TypeError)
    expect(product.bind(product, 1)).to.throw(TypeError)
  })

  it('should accept input that is a map', function () {
    expect(product.bind(product, {})).to.not.throw(TypeError)
    expect(product.bind(product, {a: 'b'})).to.not.throw(TypeError)
    expect(product.bind(product, {a: 'b', c: 'd'})).to.not.throw(TypeError)
    expect(product.bind(product, {a: 'b', c: 'd', e: [1, 2, 4]})).to.not.throw(TypeError)
  })

  it('should return same map if all values are not arrays or have single element', function () {
    var in1 = {a: 'b'}
    expect(product(in1)).to.deep.equal([in1])
    var in2 = {a: 1, b: 2, c: 3, d: 4}
    expect(product(in2)).to.deep.equal([in2])
    expect(product({a: ['b'], b: 'c'})).to.deep.equal([{a: 'b', b: 'c'}])
  })

  it('should handle maps with a single key with array value', function () {
    expect(product({a: ['b', 1, 2]})).to.deep.equal([{a: 'b'}, {a: 1}, {a: 2}])
    expect(product({'foo': ['b', 'a', 'r']})).to.deep.equal([{'foo': 'b'}, {'foo': 'a'}, {'foo': 'r'}])
  })

  it('should handle maps with two key/value pairs', function () {
    expect(product({a: ['x'], b: [1]})).to.deep.equal([{a: 'x', b: 1}])
    expect(product({a: ['x', 'y'], b: [1]})).to.deep.equal([{a: 'x', b: 1}, {a: 'y', b: 1}])
    expect(product({a: ['x', 'y'], b: [1, 2]})).to.deep.equal([{a: 'x', b: 1}, {a: 'x', b: 2}, {a: 'y', b: 1}, {a: 'y', b: 2}])
  })

  it('should handle maps with more than two key/value pairs', function () {
    expect(product({a: ['x', 'y'], b: [1], c: [1]})).to.deep.equal([{a: 'x', b: 1, c: 1}, {a: 'y', b: 1, c: 1}])
    expect(product({a: ['x', 'y'], b: [1, 2], c: 1})).to.deep.equal([{a: 'x', b: 1, c: 1}, {a: 'x', b: 2, c: 1}, {a: 'y', b: 1, c: 1}, {a: 'y', b: 2, c: 1}])
  })
})
