'use strict'

var flatten = require('lodash.flatten')
var keys = require('lodash.keys')
var map = require('lodash.map')
var reduce = require('lodash.reduce')
var values = require('lodash.values')
var zipObject = require('lodash.zipobject')

module.exports = function (elements) {
  if (elements === undefined || Array.isArray(elements) || elements.constructor !== Object) {
    throw TypeError()
  }

  return map(reduce(values(elements), function (a, b) {
    return flatten(map(a, function (x) {
      return map(Array.isArray(b) ? b : [b], function (y) {
        return x.concat([y])
      })
    }), true)
  }, [[]]), function (v) {
    return zipObject(keys(elements), v)
  })
}
